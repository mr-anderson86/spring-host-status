[![pipeline status](https://gitlab.com/mr-anderson86/spring-host-status/badges/master/pipeline.svg)](https://gitlab.com/mr-anderson86/spring-host-status/pipelines/latest)
[![code coverage](https://gitlab.com/mr-anderson86/spring-host-status/badges/master/coverage.svg)](https://gitlab.com/mr-anderson86/spring-host-status/-/jobs/artifacts/master/browse?job=test)
# Spring Host Status - a whole CI-CD process

## Description:

This repository represents a simple Java spring web application.  
Originally, initiated using this website: https://start.spring.io/ it's a very good site, highly recommended.  

Screenshots of final results can be found in the [images](images) directory :-) 

### Final otput:
* Maven artifact: the jar file which runs the web application.  
(You can find links to download it via [Packages](https://gitlab.com/mr-anderson86/spring-host-status/-/packages) on the left of this page)
* Docker image which runs the web application. (See [Packages -> Container Registry](https://gitlab.com/mr-anderson86/spring-host-status/container_registry))
* Deployment in OpenShift, so the application is already running somewhere on the web ;-)

### CI/CD process:
```mermaid
graph LR;
  test(Maven Test & Report)-->build(Maven Build & Upload);
  build-->docker(Docker Build & Push);
  docker-->openshift(Apply On OpenShift);
```

Steps:
1. Maven builds and tests the Java application.
2. The artifact (created in step 1) is uploaded to [this project's Maven repository](https://gitlab.com/mr-anderson86/spring-host-status/-/packages).  
3. Docker build image, containing the jar file created on step 1.
4. Pushes the image to [this project's Docker registry](https://gitlab.com/mr-anderson86/spring-host-status/container_registry).
5. CD - Deploys the image into a pod within some OpenShift namspace.

### Repository main files:
* The Java Spring web application (all under src)
* Dockerfile 
* GitLab Ci yml file - the CI/CD process itself.
* OpenShift template file - which deploys the application.

## Updating the code and triggering the CI:
1. Simply update the code, and commit (and push) to the repository.
2. That's it - the GitLab CI is configured to trigger CI on each commit event.

## Usage:

### Running on OpenShift:
```bash
oc login ${YOUR_OS_ADDRESS} --token=${YOUR_TOKEN}
oc project ${OS_NAMESPACE}
# The below is not must, but recommended since it shows you details before actual deployment
oc process -f os-deployment-template.yaml [-p IMAGE_TAG=${SPESIFIC_TAG}]

# Actual deployment
oc process -f os-deployment-template.yaml [-p IMAGE_TAG=${SPESIFIC_TAG}] | oc apply -f -
```

### Running Docker container locally:
```bash
# docker run -d --name <your name> -p <your port>:8085 registry.gitlab.com/mr-anderson86/spring-host-status/spring-host-status:<tag>
# Example:
docker run -d --name spring-web-app -p 80:8085 registry.gitlab.com/mr-anderson86/spring-host-status/spring-host-status:latest
```

### Running the jar file locally on the machine:
1. Download the jar file from the [Packages](https://gitlab.com/mr-anderson86/spring-host-status/-/packages) page.
2. Open CLI or CMD:
```bash
cd <dir which contains the jar file>
java -jar host-status-<version>.jar
# The above will run the application only on port 8085!!!
```

Then you can access your application by simply opening the web browser and typing the address of your machine  
(and the port number if used other than port 80)  
Of course, you can use curl as well, for example:
```bash
# If running remotely:
curl ${YOUR_ADDRESS}:${YOUR_PORT}/
# If running locally
curl localhost:${YOUR_PORT}/
```

## Other ideas:
* Install Nexus or Artifactory and deploy the jar file there.
* Then you can run the docker build stage on a different host, and pull the jar from Nexus/Artifactory.
* You can also docker push the image into your docker hub (or Nexus or others)

### The end, enjoy :)