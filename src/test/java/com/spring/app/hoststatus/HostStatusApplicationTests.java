package com.spring.app.hoststatus;
import com.spring.app.hoststatus.resource.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class HostStatusApplicationTests {

	@Test
	public void contextLoads() {
	    HostStatusApplication hsa = new HostStatusApplication();
	    assertThat(hsa, instanceOf(HostStatusApplication.class));
	    
	    HostResources hr = new HostResources();
	    String str = hr.hello();
	    assertThat(str, instanceOf(String.class));
	}

}

